package com.task5;

import javax.imageio.stream.ImageInputStream;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class TestProg {
    private static ImageInputStream br;
    public static void main(String[] args) throws IOException {
        removeComment();
    }
    private static void removeComment() throws IOException {
        try {

            // Створення буфер рідеру
            BufferedReader br = new BufferedReader(new FileReader("/home/iruna/Стільниця/CVVaskivIryna.pdf"));
            String line;
            boolean comment = false;
            while ((line = br.readLine()) != null) {
                if (line.contains("/*")) {
                    comment = true;
                    continue;
                }
                if(line.contains("*/")){
                    comment = false;
                    continue;
                }
                if(line.contains("//")){
                    continue;
                }
                if(!comment){
                    System.out.println(line);
                }
            }
            br.close();
        }

        catch (IOException e) {
            System.out.println("OOPS! File could not read!");
        }
    }
}





