package com.task4;

import java.io.*;

public class InputStreamReader {
    public static void main(String[] args) throws FileNotFoundException {
        try {

            // Зчитування даних в байтах з файлу робочого стола
            FileReader myInputStreamReader;
            myInputStreamReader = new FileReader("/home/iruna/Стільниця/CVVaskivIryna.pdf");
            String taskQA = new BufferedReader(new java.io.InputStreamReader(
                    new FileInputStream("/home/iruna/Стільниця/CVVaskivIryna.pdf"))).readLine();
            byte[] bytes = taskQA.getBytes();
            InputStreamPushBack reader = new InputStreamPushBack(new ByteArrayInputStream(bytes), bytes.length);
            double[] a = new double[16];

            // Видача даних в консолі
            byte[] readedBytes = reader.readAll (bytes);
            System.out.println("Bytes are read");
            for (byte readedByte: readedBytes) System.out.print(readedByte);

          reader.close();
        } catch (IOException e){
            e.printStackTrace();
        }

    }

    // допоміжні класи
    private static byte[] reader(byte[] bytes) {
        return bytes;
    }
    public static class  InputStreamPushBack extends PushbackInputStream {
        public InputStreamPushBack (ByteArrayInputStream in, int size) {
            super(in,size);
        }

        public byte[] readall(byte[] bytes) {
            return bytes;
        }

        public byte[] readAll(byte[] bytes) {
            return bytes;
        }
    }
}

