package com.task6;

import java.io.FileNotFoundException;
import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException {
        // допоміжний клас

        try {
            DirectoryRecurser.parseFile("/home/iruna/Стільниця");
        }

        catch (FileNotFoundException e) {
           System.out.println("File not found: " + e.getMessage());
        }
    }

}
