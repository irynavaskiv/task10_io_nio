package com.task6;

import java.io.File;
import java.io.FileNotFoundException;

public class DirectoryRecurser {
    public static void parseFile(String filePath) throws FileNotFoundException {
        File file = new File(filePath);
        if (file.exists()) {
            parseFile(file);
        } else {
            throw new FileNotFoundException(file.getPath());
        }
    }

    public static void parseFile(File file) throws FileNotFoundException {
        if (file.isDirectory()) {
            for(File child : file.listFiles()) parseFile(child);
        } else if (file.exists()) System.out.println(file.getPath());
        else {
            throw new FileNotFoundException(file.getPath());
        }
    }
}
